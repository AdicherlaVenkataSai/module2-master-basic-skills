<h1 aligin="center">CNN</h1>        
In deep learning, a convolutional neural network (CNN, or ConvNet) is a class of deep neural networks, most commonly applied to analyzing visual imagery. 

**Applications:**
-  Image and video recognition
-  Recommender systems
-  Image classification
-  Medical image analysis       
-  Natural language processing
-  Financial time series

<img align='right' src="/uploads/8931db9992b3a26bc80efbc0eca3e468/cov3d.gif" width="1000" height = "300">     

**Convolutional Neural Network has**
-  ConvNets(filtersizes = 32, 64, 128, 256, 512, 1024)      
<img src="/uploads/76de1edc958dcc953829e92381497179/dogg.gif" width="300" height = "300">       

-  Pooling layers(min, max, avg, global avg, global max)        
<img src="/uploads/444df486b427a2551d7240a9c279b2fb/0000000000000000000.gif" width="300" height = "300">        

-  Bactch Normalization
-  Activaiton Functions(ReLU, LeakyReLU Sigmoid, Softmax)

<img src="/uploads/03706a23c9af63fcf7b561167689448e/sigmoid.gif" width="300" height = "300">
<img src="/uploads/0dbb38b1cc85f003b5a2cff3ccf2f8b1/relu.gif" width="300" height = "300">
<img src="/uploads/4bcdef47f28351d57bbc15d501e6af39/softmax.gif" width="300" height = "300">



-  Flattens
-  Gradients
-  Dropouts
-  Dense layers

**CNN on MNIST**        
<img align='right' src="/uploads/6204fa35e6155f4323650c3987e5b877/ezgif.com-gif-maker__2_.gif" width="1000" height = "300">  

**[more info about CNN](https://en.wikipedia.org/wiki/Convolutional_neural_network)**



