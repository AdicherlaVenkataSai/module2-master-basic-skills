<h1 align="center">DNN</h1>

<img align='right' src="/uploads/f75be1c6b2c3c3d1fc42e8b502466809/11111.gif" width="300" height = "190">
       
A Neural Network is a network of `neurons/ artificial neurons`.Deep Neural Network is an Artificial Neural Networks with `input`, `output` layers and `hidden` layers with certain level of complexity (more than one hidden layer).

`input layer` &#8594; `hidden layers` &#8594; `output layer`        

- **Input layer** will have the trainig data, which is feed to neural network for making predictions.
- **Hidden layer** is something like black box (magic happens here).
- **Ouput layer** will make predictions based on the trainig data.

In general every node in all layers are associated with certain weights, the weights for the next layer is calulated by `z = (weight * input layer instance(node)) + bias`. The `weight` and `bias` are randomly choosen/ cosidered zero values. In order to introduce `non-linearity` in the network, we use `activation fucntions` to achieve the non-linearity, like `ReLU`, `LeakyReLU`, `Sigmoid`, `Tanh`. So the weight of the node present in next layer can be given by `z = ReLU(Σ(weight * input layer instance(node)) + bias)`. So we reach to output layer and have the respective weights for each node, the one with maximum weight is choosen as prediction (in classification).

<img align="center" src="/uploads/af6af1e32baf0017085a758b38f052f1/33333.gif" width="900" height="300">

**What if we have wrong predictions?**, thats where `Gradient Descent` comes into picture, based on the `error = predected - actual` calculated, we `back propagate` the error such that the weights gets adjust at each layer back till input layer. These iterations make `neurons/ nodes` to adapt weights and bias such that results in correct predictions.


**Note: If we have only one hidden layer then we refer it as `simple neural network`** 

<img align='center' src="/uploads/85e2586a1f267530c87bcecd9f2ce118/22222.gif" width="600" height="200">

**Types of NN**
-  [Perceptron](https://en.wikipedia.org/wiki/Perceptron)
-  [FeedForward](https://en.wikipedia.org/wiki/Feedforward_neural_network)
-  [Radial Basis Network](https://en.wikipedia.org/wiki/Radial_basis_function_network)
-  [Deep Feed Forward](https://google.com)
-  [Recurrent Neural Network](https://en.wikipedia.org/wiki/Recurrent_neural_network)
-  [Convolutional Neural Network](https://en.wikipedia.org/wiki/Convolutional_neural_network)
-  [Long/ Short Term Memory](https://en.wikipedia.org/wiki/Long_short-term_memory#:~:text=Long%20short%2Dterm%20memory%20(LSTM)%20is%20an%20artificial%20recurrent,the%20field%20of%20deep%20learning.&text=LSTM%20networks%20are%20well%2Dsuited,events%20in%20a%20time%20series.)
-  [Gated Recurrent Unit](https://en.wikipedia.org/wiki/Gated_recurrent_unit)
-  [Auto Encode](https://en.wikipedia.org/wiki/Autoencoder)

**3-D Visualisation of Neural Networks**     
![ezgif.com-gif-maker](/uploads/3dc92fa7162b637bb7b07ebd7f2275b8/ezgif.com-gif-maker.gif)
      
![ezgif.com-gif-maker__1_](/uploads/9c0a07244045888c3a7f1e2f0ed8795c/ezgif.com-gif-maker__1_.gif)

![ezgif.com-gif-maker__2_](/uploads/6204fa35e6155f4323650c3987e5b877/ezgif.com-gif-maker__2_.gif)

![ezgif.com-gif-maker__3_](/uploads/bef39c3cc98f9741c31453f7fed085ec/ezgif.com-gif-maker__3_.gif)