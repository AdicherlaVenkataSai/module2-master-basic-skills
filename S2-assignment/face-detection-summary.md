<h1 align="center"> Face Detection </h1>        
Face detection is a computer technology being used in a variety of applications that identifies human faces in digital imagesFace detection also refers to the psychological process by which humans locate and attend to faces in a visual scene.

**How Face detection works?**       
The methods used in face detection can be knowledge-based, feature-based, template matching or appearance-based. Each has advantages and disadvantages:     
-  Knowledge-based, or rule-based methods, describe a face based on rules. The challenge of this approach is the difficulty of coming up with well-defined rules.
-  Feature invariant methods -- which use features such as a person's eyes or nose to detect a face can be negatively affected by noise and light.
-  Template-matching methods are based on comparing images with standard face patterns or features that have been stored previously and correlating the two to detect a face.
-  Appearance-based methods employ statistical analysis and machine learning to find the relevant characteristics of face images. This method, also used in feature extraction for face recognition, is divided into sub-methods.

**more info about face detection [link1](https://www.researchgate.net/publication/326667118_Face_Detection_Techniques_A_Review), [link2](https://www.sciencedirect.com/topics/computer-science/face-detection#:~:text=Some%20reported%20systems%20use%20traditional,contain%20strong%20edges%20and%20are) .**

**Note: Used the `haar cascades` to detect the face in the given live camera feed. We can also use Histogram of Oriented Gradients(HOG) or Multi-task Cascaded Convolutional Networks (MTCNN)**     
**Practical Implementation Result (self face detection)** || You can try yours too [Complete Code](https://github.com/AdicherlaVenkataSai/open-source/tree/master/1.%20Face%20Detection)        
The code is Implemented and deployed using flask (can host the same application using heroku)      
![testing111-min](https://user-images.githubusercontent.com/26376075/93674954-68181280-fac9-11ea-9334-bc0765f2389c.gif)

**Just for reference wrt to [HOG]() and [MTCNN](https://towardsdatascience.com/face-detection-using-mtcnn-a-guide-for-face-extraction-with-a-focus-on-speed-c6d59f82d49)**   

<img src="/uploads/26e5c70b0718c2a5433836a6f77cf4e6/hog.gif" width="420" height = "300">   

<img src="/uploads/fd73b91a8775f08617921d6db4d04baf/mctnn.gif" width="420" height = "300">  
